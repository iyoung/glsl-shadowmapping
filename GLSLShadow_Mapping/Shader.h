#pragma once

#include <GL\glew.h>
#include <memory>
#include <glm.hpp>
#include "defines.cpp"

class ShaderProgram
	{
	public:
		ShaderProgram();

		//////////////////////////////////////////////////////////////////////
		//	operation methods
		//	tell openGL to turn this program on or off
		//////////////////////////////////////////////////////////////////////
		void activate();
		void deactivate();

		//////////////////////////////////////////////////////////////////////
		//	creational methods, used by Shader to set uniform locations,
		//	for passing data to the program
		//////////////////////////////////////////////////////////////////////
		void setModelMatrixUniformLocation(GLuint pUniformLocation)			{    mModelMatrixLocation = pUniformLocation;      }
		void setViewMatrixUniformLocation(GLuint pUniformLocation)			{    mViewMatrixLocation = pUniformLocation;       } 
		void setProjectionMatrixUniformLocation(GLuint pUniformLocation)	{    mProjectionMatrixLocation = pUniformLocation; } 
		void setNormalMatrixUniformLocation(GLuint pUniformLocation)		{    mNormalMatrixLocation = pUniformLocation;     }
		void setShaderProgramLocation(GLuint pProgramLocation)				{    mProgramLocation = pProgramLocation;          }

		//////////////////////////////////////////////////////////////////////
		//	operational methods, for passing data to the shader program
		//	at run time
		//////////////////////////////////////////////////////////////////////
		void setModelMatrix(mat4& pModelMatrix);
		void setViewMatrix(mat4& pViewMatrix);
		void setProjectionMatrix(mat4& pProjectionMatrix);
		void setNormalMatrix(mat3& pNormalMatrix);

		//////////////////////////////////////////////////////////////////////
		//	sets final draw call to render geometry to back buffer or 
		//	render target texture
		//////////////////////////////////////////////////////////////////////
		void render(GLenum pType, GLuint pVertexCount, GLuint pIndex, GLuint pMeshLocation);
		virtual ~ShaderProgram()											{    glDeleteProgram(mProgramLocation);             }
	protected:
		GLuint mModelMatrixLocation;
		GLuint mViewMatrixLocation;
		GLuint mProjectionMatrixLocation;
		GLuint mNormalMatrixLocation;
		GLuint mProgramLocation;
	};
class Shader
	{
	public:
		enum AttributeType
			{
			ATTRIBUTE_VERTEX,
			ATTRIBUTE_COLOUR,
			ATTRIBUTE_NORMAL,
			ATTRIBUTE_TEX_COORD,
			ATTRIBUTE_TANGENT,
			ATTRIBUTE_BITANGENT
			};

		static GLuint loadShader(const char* pShaderFile,  GLenum pShaderType);
		static std::unique_ptr<ShaderProgram> createShaderProgram(const char* pVertexShader, const char* pFragmentShader);
		static void printShaderError(GLuint pShader);
	};


