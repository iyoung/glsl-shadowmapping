#pragma once
#include "application.h"
#include <GL\glew.h>
#include <SDL.h>
#include <time.h>
#include <memory>
#include "Camera.h"

class ShaderProgram;

class GLApp :
	public Application
	{
	public:
		GLApp(void);
		virtual void start();
		virtual void shutdown();
		virtual void update();
		virtual bool isRunning();
		virtual ~GLApp(void);
	protected:
		void createSDLGLWindow();
		void parseInputEvents();
		void render();
		void exitFatalError(const char* pErrMsg);
		bool mRunning;
		SDL_GLContext mContext;
		SDL_Window* mWindow;
		std::shared_ptr<Camera> mCamera;
		float mDeltaTime;
		clock_t mLastTime;
		float mShutdownTimer;
		std::unique_ptr<ShaderProgram> mProgram;
	};

