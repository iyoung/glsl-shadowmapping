#version 330 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

in vec3 in_Position;
in vec3 in_Colour;

out VS_FS_INTERFACE
{
	vec4 colour;
} block;

void main (void)
{
	mat4 mvp = modelMatrix * viewMatrix * projectionMatrix;
	block.colour = vec4(in_Colour,1.0);

	gl_Position = mvp * vec4 (in_Position,1.0);
}