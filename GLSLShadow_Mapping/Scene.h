#pragma once
#include <vector>
#include <memory>
class SceneObject;
class Camera;
class Scene
	{
	public:
		Scene(void);
		virtual void update(float pDeltaTime);
		virtual void render();
		void addObject(std::unique_ptr<SceneObject> pObject);
		void addCamera(std::shared_ptr<Camera> pCamera);
		virtual ~Scene(void);
	protected:
		void renderObject(std::unique_ptr<SceneObject>& pObj);
		std::vector<std::unique_ptr<SceneObject>> mObjects;
		std::shared_ptr<Camera> mCamera;
		mat4 mView;
		mat4 mProjection;
	};

