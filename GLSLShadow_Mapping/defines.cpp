#include <glm.hpp>
typedef glm::vec3 vec3;
typedef glm::mat4 mat4;
typedef glm::mat3 mat3;
//macro defining value of PI, divided by 180 degrees, for converting degrees to radians
#define PIOVER180 0.017453293;