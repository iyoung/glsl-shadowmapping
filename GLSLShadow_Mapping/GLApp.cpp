#include "GLApp.h"
#include <iostream>
#include "Shader.h"

GLApp::GLApp(void)
{
	mWindow = nullptr;
	mContext = nullptr;
	mRunning = false;
	mDeltaTime = 0.0f;
	mShutdownTimer = 0.0f;
}
void GLApp::exitFatalError(const char* pErrMsg)
{
	std::cout << pErrMsg << " ";
    exit(1);
}
void GLApp::start()
{
	mShutdownTimer = 5.0f;
	mRunning = true;
	mLastTime = 0;
	createSDLGLWindow();
	mProgram = Shader::createShaderProgram("basic.vs.glsl","basic.fs.glsl");
}
void GLApp::update()
{
	clock_t currentTime = clock();
	mDeltaTime = currentTime - mLastTime;
	mDeltaTime /= CLOCKS_PER_SEC;

	mShutdownTimer -= mDeltaTime;

	if(mShutdownTimer <= 0.0f)
		shutdown();

	parseInputEvents();

	mLastTime = currentTime;
	renderScene();
}
void GLApp::shutdown()
{
	mRunning = false;
}
bool GLApp::isRunning()
{
	return mRunning;
}
void GLApp::createSDLGLWindow()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        mRunning = false; 
	  
    // Request an OpenGL 3.3 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    mWindow = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!mWindow) // Check window was created OK
        mRunning = false;
 
    mContext = SDL_GL_CreateContext(mWindow); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
		// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;
}
void GLApp::parseInputEvents()
{
	SDL_Event sdlEvent; 
	SDL_PollEvent(&sdlEvent);
	while (SDL_PollEvent(&sdlEvent)) {
		if (sdlEvent.type == SDL_QUIT)
			shutdown();
		//else
			//controller->parseSDLEvent(sdlEvent);
	}
}
void GLApp::render()
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	//draw calls & matrix calculations go here

	SDL_GL_SwapWindow(mWindow);
}
GLApp::~GLApp(void)
{
    SDL_GL_DeleteContext(mContext);
	SDL_DestroyWindow(mWindow);
	
}
