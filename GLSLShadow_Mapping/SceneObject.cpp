#include "SceneObject.h"
#include <gtc/matrix_transform.hpp>

SceneObject::SceneObject(void)
	{
	mChanged = false;
	}
void SceneObject::setPosition(vec3 pPosition)
	{
	mPosition = pPosition;
	mChanged = true;
	}
void SceneObject::setOrientation(vec3 pOrientation)
	{
	mOrientation = pOrientation;
	mChanged = true;
	}
void SceneObject::setScale(vec3 pScale)
	{
	mScale = pScale;
	mChanged = true;
	}

void SceneObject::setTransform()
	{
	if(mChanged)
		{
			mTransform = mat4(1.0);
			mTransform = glm::translate(mTransform,mPosition);
			mTransform = glm::rotate(mTransform,1.0f,mOrientation);
			mTransform = glm::scale(mTransform,mScale);
			mChanged = false;
		}
	}

SceneObject::~SceneObject(void)
	{
	}
