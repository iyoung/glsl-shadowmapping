#include "Scene.h"
#include "SceneObject.h"
#include "Camera.h"


Scene::Scene(void)
	{
	}

void Scene::update(float pDeltaTime)
	{
	for(auto iter = mObjects.begin();iter != mObjects.end(); iter++)
		{
		(*iter)->setTransform();
		}
	}
void Scene::render()
	{
		//shadow pass
		//renderer->enableShadowPass();
		//renderer->setShadowSource(light);
		auto iter = mObjects.begin();
		while(iter != mObjects.end())
			{
				renderObject((*iter));
				iter++;
			}
		//regular pass
		//renderer->enableFinalPass();
		//renderer->setLightSource(light);
		iter = mObjects.begin();
		while(iter != mObjects.end())
			{
				renderObject((*iter));
				iter++;
			}
	}
void Scene::addObject(std::unique_ptr<SceneObject> pObject)
	{
		mObjects.push_back(pObject);
	}
void Scene::addCamera(std::shared_ptr<Camera> pCamera)
	{
		mCamera=pCamera;
	}
void Scene::renderObject(std::unique_ptr<SceneObject>& pObj)
	{
		mat4 mvp = pObj->getTransform() * mView * mProjection;
		//bind object model(object meshID, object vertex/index count, triangles, starting index = 0)
		//enable shader
		//set light
		//do draw call
	}

Scene::~Scene(void)
	{
	}
