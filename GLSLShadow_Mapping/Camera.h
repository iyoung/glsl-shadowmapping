#pragma once
#include "defines.cpp"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Flexible camera class, using Euler angles to create a view transform matrix								 //
// Example Use:																								 //
// camera.MoveForward(moveSpeed*deltaTime);																	 //
// camera.RotatePitch(rotateSpeed*deltaTime);																 //
// camera.getViewTransform(ViewMatrix);																		 //
// camera.MoveTo(targetPosition);																			 //
// camera.LookAt(object.getPosition(),viewingDistance);		                                                 //
//																											 //
// To rotate around an object in third person:																 //
// camera.Reset();									all previous settings are useless now					 //
// camera.RotateYaw(object.getHorizOrientation);	camera must be set to look the same direction as object	 //
// camera.RotatePitch(-preferredVerticalAngle);		Looking down onto object								 //
// camera.LookAt(object.getPosition(),preferredFollowDistance);			set camera to look at object		 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//simple struct for storing Euler angles, and position
struct EulerAngle
{
	vec3 m_Position;
	float m_RollAngle;
	float m_PitchAngle;
	float m_YawAngle;
};
enum ProjectionType
	{
		PROJECT_ORTHO,
		PROJECT_PERSPECTIVE
	};
//The camera class
class Camera
{
public:
	Camera(void);
	~Camera(void);
	//Move the camera forward along it's local Forward vector
	void MoveForward(float p_MoveFactor);
	//Pan camera along it's local Right vector
	void MoveRight(float p_MoveFactor);
	//Pan camera along it's local Up vector
	void MoveUp(float p_MoveFactor);
	//Move camera along global X axis
	void MoveX(float p_MoveFactor);
	//Move camera along global Y axis
	void MoveY(float p_MoveFactor);
	//Move camera along global Z axis
	void MoveZ(float p_MoveFactor);
	//Generates and normalizes Forward and Right Vectors
	void Update();
	///////////////////////////////////////////////////////////////////////////////
	//	Rotate Functions take in an angle in degrees and converts it to Radians  //
	///////////////////////////////////////////////////////////////////////////////
	//rotates camera up/down locally
	void RotatePitch(float m_RotateAngle);
	//rotate camera left/right locally
	void RotateYaw(float m_RotateAngle);
	//rotate camera clockwise/anticlockwise locally
	void RotateRoll(float m_RotateAngle);
	//generates view matrix, used for calculating view space transformations
	void getViewMatrix(mat4& p_ViewMatrix, bool p_RotationOnly);
	//sets projection matrix used for calculating screen space transformations
	void getProjectionMatrix(mat4& pProjectionMatrix);
	//sets projection between ortho and perspective mode
	void setProjectionType(ProjectionType pType);
	void setVieldofView(const float& pFOVAngle);
	void setVPWidth(const int& pScrWidth);
	void setVPHeight(const int& pScrHeight);
	void setDrawDistance(const float& pDrawDist);
	//returns the position of the camera, in cartesian coordinates
	glm::vec3 getPosition();
	//moves camera to a target location in cartesian space
	void MoveTo(const vec3& p_NewPosition);
	//moves camera to view a specific point, without rotation
	void LookAt(const vec3& p_Point, const float& p_Distance);
	//this method levels out the camera, zeroing vertical angle and roll angle
	void LevelCamera();
	//completely resets camera
	void Reset();
private:
	//stored Euler orientation
	EulerAngle m_Orientation;
	//directions used for movement
	vec3 m_Direction;
	vec3 m_Right;
	float mDrawDistance;
	float mFieldofView;
	int mVPWidth;
	int mVPHeight;
	ProjectionType mType;
};
