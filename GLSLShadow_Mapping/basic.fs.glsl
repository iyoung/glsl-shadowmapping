#version 330 core

in VS_FS_INTERFACE
{
	vec4 colour;
} block;

layout (location = 0) out vec4 out_colour;

void main(void)
{
	out_colour = block.colour;
}