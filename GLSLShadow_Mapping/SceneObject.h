#pragma once
#include <glm.hpp>
#include "defines.cpp"

class SceneObject
	{
	public:
		SceneObject(void);
		void setPosition(vec3 pPosition);
		void setOrientation(vec3 pOrientation);
		void setScale(vec3 pScale);
		vec3 getPosition()								{return mPosition;				}
		vec3 getOrientation()							{return mOrientation;			}
		vec3 getScale()									{return mScale;					}
		bool hasChanged()								{return mChanged;				}
		mat4 getTransform()								{return mTransform;				}
		void setTransform();
		virtual ~SceneObject(void);
	protected:
		vec3 mPosition;
		vec3 mOrientation;
		vec3 mScale;
		bool mChanged;
		mat4 mTransform;
	};

