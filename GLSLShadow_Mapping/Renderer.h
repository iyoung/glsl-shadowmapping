#pragma once
#include <GL\glew.h>
#include "defines.cpp"

class Renderer
	{
	public:
		Renderer(void);
		void enableShadowPass();
		void enableFinalPass();
		void setShadowSource();
		void setLightSource();
		~Renderer(void);
	private:
		
	};

