#pragma once
class Application
	{
	public:
		Application(void);
		virtual void start() = 0;
		virtual void shutdown() = 0;
		virtual void update() = 0;
		virtual bool isRunning() = 0;
		virtual ~Application(void);
	};

