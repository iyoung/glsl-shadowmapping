#include "Shader.h"
#include <gtc/type_ptr.hpp>
#include <iostream>
#include "FileIO.h"

#define MATH_DATA glm::value_ptr

GLuint Shader::loadShader(const char* pShaderFile, GLenum pShaderType)
	{
		GLint shaderLocation = 0;
		GLint size = 0;

		char* shader = FileIO::loadFile(pShaderFile,size);
		const char * shaderStage = shader;

		shaderLocation = glCreateShader(pShaderType);
		glShaderSource(shaderLocation, 1, &shaderStage, &size);
		
		GLint compiled;

		glCompileShader(shaderLocation);

		glGetShaderiv(shaderLocation, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			if (pShaderType == GL_VERTEX_SHADER)
				std::cout << " vertex shader not compiled." << std::endl;

			else if (pShaderType == GL_FRAGMENT_SHADER)
				std::cout << " fragment shader not compiled." << std::endl;

			else if (pShaderType == GL_GEOMETRY_SHADER)
				std::cout << " geometry shader not compiled." << std::endl;

			else if (pShaderType == GL_COMPUTE_SHADER)
				std::cout << " compute shader not compiled." << std::endl;

			Shader::printShaderError(shaderLocation);
		}
		return shaderLocation;
	}


std::unique_ptr<ShaderProgram> Shader::createShaderProgram(const char* pVertexShader, const char* pFragmentShader)
	{
	GLuint p = 0;

	GLuint vs = Shader::loadShader(pVertexShader,GL_VERTEX_SHADER);

	if(vs == 0)
		return nullptr;

	GLuint fs = Shader::loadShader(pFragmentShader, GL_FRAGMENT_SHADER);

	if(fs == 0)
		{
		glDeleteShader(vs);
		return nullptr;
		}


	//instantiate a shader object
	std::unique_ptr<ShaderProgram> shader = std::unique_ptr<ShaderProgram>(new ShaderProgram());

	//create the program
	p = glCreateProgram();

	//attach the shader stages to the program
	glAttachShader(p,vs);
	glAttachShader(p,fs);

	//bind attribute locations
	glBindAttribLocation(p,ATTRIBUTE_VERTEX,"in_Position");
	glBindAttribLocation(p,ATTRIBUTE_COLOUR,"in_Colour");
	glBindAttribLocation(p,ATTRIBUTE_NORMAL,"in_Normal");
	glBindAttribLocation(p,ATTRIBUTE_TEX_COORD,"in_TexCoord");
	glBindAttribLocation(p,ATTRIBUTE_TANGENT,"in_Tangent");
	glBindAttribLocation(p,ATTRIBUTE_BITANGENT,"in_BiTangent");

	//and link the program
	glLinkProgram(p);
	//set the program location and activate it
	shader->setShaderProgramLocation(p);
	shader->activate();

	//bind uniform locations
	GLint M = 0;
	GLint V = 0;
	GLint P = 0;
	GLint N = 0;

	//query the shader to acquire basic uniform locations
	M = glGetUniformLocation(p,"modelMatrix");
	V = glGetUniformLocation(p,"viewMatrix");
	P = glGetUniformLocation(p,"projectionMatrix");
	N = glGetUniformLocation(p,"normalMatrix");

	//and set the values in the shasder program object
	shader->setModelMatrixUniformLocation(M);
	shader->setViewMatrixUniformLocation(V);
	shader->setProjectionMatrixUniformLocation(P);
	shader->setNormalMatrixUniformLocation(N);

	//program created correctly, so deactivate, and return pointer, as it is ready for use.
	shader->deactivate();

	return shader;
	}

void Shader::printShaderError(const GLuint pShader) 
	{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(pShader))
		glGetProgramiv(pShader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(pShader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) 
		{ // If message has some contents
		logMessage = new GLchar[maxLength];

		if (!glIsShader(pShader))
			glGetProgramInfoLog(pShader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(pShader,maxLength, &logLength, logMessage);

		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete [] logMessage;
		}
	// should additionally check for OpenGL errors here
	}





ShaderProgram::ShaderProgram()
	{
		mModelMatrixLocation = 0;
		mViewMatrixLocation = 0;
		mProjectionMatrixLocation = 0;
		mNormalMatrixLocation = 0;
		mProgramLocation = 0;
	}

void ShaderProgram::activate()
	{
	glUseProgram(mProgramLocation);
	}

void ShaderProgram::deactivate()
	{
	glUseProgram(0);
	}

void ShaderProgram::setModelMatrix(mat4& pModelMatrix)
	{
	glUniformMatrix4fv(mModelMatrixLocation, 1, GL_FALSE,MATH_DATA(pModelMatrix));
	}

void ShaderProgram::setViewMatrix(mat4& pViewMatrix)
	{
	glUniformMatrix4fv(mViewMatrixLocation, 1, GL_FALSE,MATH_DATA(pViewMatrix));
	}

void ShaderProgram::setProjectionMatrix(mat4& pProjectionMatrix)
	{
	glUniformMatrix4fv(mProjectionMatrixLocation, 1, GL_FALSE,MATH_DATA(pProjectionMatrix));
	}

void ShaderProgram::setNormalMatrix(mat3& pNormalMatrix)
	{
	glUniformMatrix3fv(mModelMatrixLocation, 1, GL_FALSE,MATH_DATA(pNormalMatrix));
	}

void ShaderProgram::render(GLenum pType, GLuint pVertexCount, GLuint pIndex, GLuint pMeshLocation)
	{
	glBindVertexArray(pMeshLocation);	// Bind mesh VAO
	glDrawElements(pType, pVertexCount,  GL_UNSIGNED_INT, 0);	// draw VAO 
	glBindVertexArray(0);
	}

