#include "Camera.h"
#include <gtc\matrix_transform.hpp>
//constructor
Camera::Camera(void)
{
	m_Orientation.m_PitchAngle = 0.0f;
	m_Orientation.m_YawAngle = 0.0f;
	m_Orientation.m_RollAngle = 0.0f;

	m_Orientation.m_Position = vec3(0.0);
	Update();
}
//generates vectors necessary for panning camera
void Camera::Update()
{
		//convert stored Euler angles into cartesian vectors, to calculate Forward direction
	m_Direction = glm::normalize(		vec3(	cos(m_Orientation.m_PitchAngle) * sin(m_Orientation.m_YawAngle),
												sin(m_Orientation.m_PitchAngle),
												cos(m_Orientation.m_PitchAngle) * cos(m_Orientation.m_YawAngle)));
	//and perpendicular Right direction
	m_Right = glm::normalize(		vec3(	sin(m_Orientation.m_YawAngle - 3.141592/2.0f),
											0.0f,
											cos(m_Orientation.m_YawAngle - 3.141592/2.0f)));
	//Up direction is only needed for view matrix generation, so no need to calculate it here.
}
void Camera::getViewMatrix(mat4& p_ViewMatrix, bool p_RotationOnly)
{
	glm::vec3 v_Up = glm::cross(m_Right,m_Direction);
	//of all we want is the rotation matrix, 
	if(p_RotationOnly)
	{
		p_ViewMatrix = mat4(mat3(glm::lookAt(m_Orientation.m_Position,m_Orientation.m_Position+m_Direction,v_Up)));
	}
	else
	{
		p_ViewMatrix =glm::lookAt(m_Orientation.m_Position,m_Orientation.m_Position+m_Direction,v_Up);
	}
}
//local movement methods, relative to camera orientation
void Camera::MoveForward(float p_MoveFactor)
{
	m_Orientation.m_Position+=m_Direction*p_MoveFactor;
}
void Camera::MoveRight(float p_MoveFactor)
{
	m_Orientation.m_Position+=m_Right*p_MoveFactor;
}
void Camera::MoveUp(float p_MoveFactor)
{
	//cross product of direction and right vectors is the local Up vector
	m_Orientation.m_Position += glm::cross(m_Right,m_Direction)*p_MoveFactor;
}
//local rotation methods
void Camera::RotateYaw(float p_RotateAngle)
{
	//convert to radians and increment stored horizontal angle
	m_Orientation.m_YawAngle+=p_RotateAngle*PIOVER180;
	Update();
}
void Camera::RotatePitch(float p_RotateAngle)
{
	//conevrt to radians and increment stored vertical angle
	m_Orientation.m_PitchAngle+=p_RotateAngle*PIOVER180;
	Update();
}
void Camera::RotateRoll(float p_RotateAngle)
{
	//convert to radians and increment stored roll angle
	m_Orientation.m_RollAngle+=p_RotateAngle*PIOVER180;
	Update();
}
//This set of methods move camera around in cartesian space, disregarding local orientation
void Camera::MoveX(float p_MoveFactor)
{
	m_Orientation.m_Position.x+=p_MoveFactor;
}
void Camera::MoveY(float p_MoveFactor)
{
	m_Orientation.m_Position.y+=p_MoveFactor;
}
void Camera::MoveZ(float p_MoveFactor)
{
	m_Orientation.m_Position.z+=p_MoveFactor;
}
//returns position of camera
glm::vec3 Camera::getPosition()
{
	return m_Orientation.m_Position;
}
//moves camera to a new position in cartesian space
void Camera::MoveTo(const vec3& p_NewPosition)
{
	m_Orientation.m_Position = p_NewPosition;
}
//moves the camera to look at a point in cartesian space, without rotation, from a specified distance
void Camera::LookAt(const vec3& p_Point, const float& p_Distance)
{
	MoveTo(p_Point-(m_Direction*p_Distance));
}
//levels out the camera, in case it finds itself upside down
void Camera::LevelCamera()
{
	m_Orientation.m_PitchAngle = 0.0f;
	m_Orientation.m_RollAngle = 0.0f;
	Update();
}
//resets camera to "factory settings"
void Camera::Reset()
	{
		//set all angles to zero
		m_Orientation.m_PitchAngle = 0.0f;
		m_Orientation.m_YawAngle = 0.0f;
		m_Orientation.m_RollAngle = 0.0f;
		//send camera back to cartesian origin
		m_Orientation.m_Position = vec3(0.0);
	}
void Camera::setProjectionType(ProjectionType pType)
	{
		mType = pType;
	}
void Camera::setVieldofView(const float& pFOVAngle)
	{
		mFieldofView = pFOVAngle;
	}
void Camera::setVPHeight(const int& pScrHeight)
	{
		mVPHeight = pScrHeight;
	}
void Camera::setVPWidth(const int& pScrWidth)
	{
		mVPWidth = pScrWidth;
	}
void Camera::getProjectionMatrix(mat4& pProjectionMatrix)
	{
		if(mType == PROJECT_ORTHO)
			pProjectionMatrix = glm::ortho(0,mVPWidth,0,mVPHeight);
		else
			pProjectionMatrix = glm::perspective(mFieldofView,(float)mVPHeight/(float)mVPWidth,1.0f,mDrawDistance);
	}
void Camera::setDrawDistance(const float& pDrawDist)
	{
		mDrawDistance = pDrawDist;
	}

Camera::~Camera(void)
	{
	}
