#include "Application.h"
#include "GLApp.h"
#include <memory>


int main (int argc, char *argv[])
{
	std::unique_ptr<Application> app = std::unique_ptr<GLApp>( new GLApp() );
	app->start();
	while (app->isRunning())
	{
		app->update();
	}	
	app->shutdown();
	return 0;
}